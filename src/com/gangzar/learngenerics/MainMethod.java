package com.gangzar.learngenerics;

public class MainMethod {
    public static void main(String[] args) {
        FootBallPlayer footBallPlayer = new FootBallPlayer("Agas");
        SoccerPlayer soccerPlayer = new SoccerPlayer("Gangzar");
        BaseBallPlayer baseBallPlayer = new BaseBallPlayer("Sulaeman");

        Team <FootBallPlayer> gangzarTeamSport = new Team<>(" Gangzar Sport");
        gangzarTeamSport.addPlayer(footBallPlayer);
//        gangzarTeamSport.addPlayer(soccerPlayer);
//        gangzarTeamSport.addPlayer(baseBallPlayer);

        System.out.println(gangzarTeamSport.numPlayer());

        Team <BaseBallPlayer> gangzarBaseballTeam = new Team<>("Gangzar Base ball team");
        gangzarBaseballTeam.addPlayer(baseBallPlayer);


        Team <SoccerPlayer> gangzarSoccerPlayer = new Team<>("Gangzar soccer player");
        gangzarSoccerPlayer.addPlayer(soccerPlayer);

        Team<FootBallPlayer> manchesterUnited = new Team<>("Manchester United");
        Team<FootBallPlayer> scunthropeUnited = new Team<>("Scunthrope United");

        manchesterUnited.matchResult(scunthropeUnited,1,0);
        scunthropeUnited.matchResult(manchesterUnited,0,1);

        System.out.println("Ranking");
        System.out.println(manchesterUnited.getName() + " : " +manchesterUnited.ranking());

        System.out.println(manchesterUnited.compareTo(scunthropeUnited));
    }
}

