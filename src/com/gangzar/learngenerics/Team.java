package com.gangzar.learngenerics;

import java.util.ArrayList;

public class Team<T extends Player> implements Comparable<Team<T>> {
    private String name;
    int played = 0;
    int menang = 0;
    int kalah = 0;
    int seri = 0;

    private ArrayList<T> member = new ArrayList<>();

    public Team(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public boolean addPlayer(T player) {
        if (member.contains(player)) {
            System.out.println(player.getName() + " al ready in your team");
            return false;
        } else {
            member.add(player);
            System.out.println(player.getName() + " picked for team " + this.name);
            return true;
        }
    }

    public int numPlayer() {
        return this.member.size();
    }

    public void matchResult(Team opponent, int ourScore, int theirScore) {
        String message;
        if (ourScore > theirScore) {
            menang++;
            message = " beat";
        } else if (ourScore == theirScore) {
            seri++;
            message = " drew with";
        } else {
            kalah++;
            message = " lost to";
        }
        played++;
        if (opponent != null) {
            System.out.println(this.getName() + message + opponent.getName());
            opponent.matchResult(null, ourScore, theirScore);
        }
    }

    public int ranking() {
        return (menang * 2) + seri;
    }

    @Override
    public int compareTo(Team<T> team) {
        if (this.ranking() > team.ranking()) {
            return -1;
        } else if (this.ranking() < team.ranking()) {
            return 1;
        } else {
            return 0;
        }
    }
}
